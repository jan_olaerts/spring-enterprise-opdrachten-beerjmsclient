package be.janolaerts.enterprisespringopdrachten.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import javax.jms.ObjectMessage;

@Component
public class BeerJmsSender {

    private JmsTemplate jmsTemplate;

    @Autowired
    public void setJmsTemplate(JmsTemplate jmsTemplate) {
        this.jmsTemplate = jmsTemplate;
    }

    // Todo: not sure how this must be done
    public void orderBeers(final String name, final int[][] beers) {
        jmsTemplate.convertAndSend("BeerQueue", name);
        jmsTemplate.convertAndSend("BeerQueue", beers);

        jmsTemplate.send("BeerQueue", s -> s.createObjectMessage(beers));
    }
}