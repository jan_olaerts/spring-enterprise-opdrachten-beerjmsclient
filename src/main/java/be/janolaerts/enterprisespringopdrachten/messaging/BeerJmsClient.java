package be.janolaerts.enterprisespringopdrachten.messaging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BeerJmsClient {

    public static void main(String[] args) throws Exception {

        ConfigurableApplicationContext ctx =
                SpringApplication.run(BeerJmsClient.class, args);

        BeerJmsSender sender = ctx.getBean(BeerJmsSender.class);
        sender.orderBeers("Homer", new int[][]{{10, 50}, {11, 50}});
    }
}